var url = require('url');
var fs = require('fs');
var datos = require('./scripts/datos');
var api = require('./scripts/API');

function renderHTML(path, response) {
    fs.readFile(path, null, function(error, data) {
        if (error) {
            response.writeHead(404);
            response.write('File not found!');
        } else {
            response.write(data);
        }
        response.end();
    });
}

function reponse_write(path, response){
    if(path.includes("get_divisa_data")){
      response.write(JSON.stringify(datos.divisas_data));
      response.end();
      return true;
    }
    else if(path.includes("get_divisas_inciales")){
      response.write(datos.divisas_iniciales.toString());
      response.end();
      return true;        
    }

    else if(path.includes("getApi")){
      response.write(api.ejecutar_api(path).toString());
      response.end();
      return true;        
    }
    else{
      return false;
    }  
}

function set_content_type(path,response){
    if (path.includes("css")){
      response.writeHead(200, {'Content-Type': 'text/css'});
    }
    else if(path.includes("svg")){
      response.setHeader('Content-Type', 'text/svg+xml');
    }
    else{
      response.writeHead(200, {'Content-Type': 'text/html'});
    }
}

function draw_page(path,response){
  switch (path) {
    case '/':
        console.log('Servidor Encendido');
        renderHTML('./index.html', response);
        break;
    
    default:        
        renderHTML("."+path, response);
        break;
        response.writeHead(404);
        response.write('Route not defined');
        response.end();
  }
}

module.exports = {

  handleRequest: function(request, response) {

    var path = url.parse(request.url).pathname;
    
    if(reponse_write(path, response)){
      return;
    }

    set_content_type(path,response);
    draw_page(path,response);

  }  

};