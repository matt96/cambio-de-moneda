//---------------------- funcion para ocultar una sección-div ---------------------- 

function mostrar_ocultar($id_div) {
    $($id_div).animate({
      bottom: "+=600",
      height: "toggle",
      opacity: "toggle"
    }, {
      duration: 1000,
      queue: false
    });    
}

// ---------------------- peticiones de datos ----------------------
function get_divisa_data(){
    var peticion = new XMLHttpRequest();
    peticion.open('GET',"get_divisa_data", false);
    peticion.send();
    var respuesta = JSON.parse(peticion.responseText);
    return respuesta;
}

function get_divisas_inciales(){
    var peticion = new XMLHttpRequest();
    peticion.open('GET',"get_divisas_inciales", false);
    peticion.send();
    var respuesta = peticion.responseText.split(',');
    return respuesta;
}

function get_api($source, $target, $quantity){
    var peticion = new XMLHttpRequest();
    var str = "getApi"+"&"+$source+"&"+$target+"&"+$quantity;
    peticion.open('GET',str, false);
    peticion.send();
    var respuesta = peticion.responseText;
    return respuesta;    
}

// --------------------- Cargar de pagina --------------------
function load_page(){
    
    mostrar_ocultar('#origen'); 
    cargar_panel_divisas_general()   
    seleccionar_divisas_iniciales();
    copy_origen_checkbox();
    tomar_divisas_conversion(); 
}

function cargar_panel_divisas_general(){
    var divisas_data =get_divisa_data();
    for(var divisa in divisas_data){
        var descripcion = " | "+divisa+" | "+divisas_data[divisa];
        add_check_box("1",divisa, descripcion, "valores-origen", "origen-checkbox");
    }
}

function seleccionar_divisas_iniciales(){
    var divisas_iniciales = get_divisas_inciales();
    for (var divisa in divisas_iniciales){
        document.getElementById("1"+divisas_iniciales[divisa]).setAttribute("checked", "checked");
    }
}

// --------------------- Funciones de manejo de divisas --------------------

//copia las divisas seleccionadas a evaluar
function copy_origen_checkbox(){
    $('.origen-checkbox:checked').each(
        function() {
            if (!$("#"+"2"+$(this).val()).length > 0){
                var divisas_data =get_divisa_data();
                var pais = divisas_data[$(this).val()];
                var descripcion = " | "+$(this).val()+" | "+pais;
                add_check_box("2",$(this).val(), descripcion, "divisas", "divisas-checkbox");
            } 
        }
    );    
}

//elimina divisas seleccionadas a evaluar 
function delete_divisas_checkbox(){
    $('.divisas-checkbox:checked').each(
        function() {
            remove_checkbox("divisas", "label-"+"2"+$(this).val(), "2"+$(this).val());
        }
    );    
}

//remueve una divisa del panel de divisasa evalar
function remove_checkbox($container, $label_id, $checkbox_id){
    var label = document.getElementById($label_id);
    var checkbox = document.getElementById($checkbox_id);
    document.getElementById($container).removeChild(label);
    document.getElementById($container).removeChild(checkbox);
}

// cargar el panel de conversion con las divosas a evaluar
function tomar_divisas_conversion(){
    $("#conversiones").empty();
    $('.divisas-checkbox').each(
        function() {
            add_text_box($(this).val(),"conversiones","textbox_divisa");
        }
    );    
}

// --------------------- Manejo de checkbox y textbox --------------------

// selecciona la bandera del del pais basado en un css, modifca las clases 
function get_litlle_class_flag($divisa){
    
    var lower_divisa = $divisa;
    lower_divisa = lower_divisa.toLowerCase();
    var first_part = "flag-icon-background flag-icon flag-icon-";
    var new_divisa =lower_divisa.substr(0, lower_divisa.length-1);
    var second_part = " flag-icon-squared";
    var full_string_class = first_part + new_divisa +second_part;
    return full_string_class;
} 

// crea una eqtiueta con bandera
function crear_flag_label($id_value, $value, $description, $classes){
    // crea la etiqueta asociada
    var label= document.createElement("label");
    label.className += $classes;
    label.setAttribute("id", "label-"+$id_value+$value);
    // crea la descripcion de la etiqueta
    var description = document.createTextNode($description);
    // crea area de la badenra
    var span = document.createElement("span");    
    span.className += get_litlle_class_flag($value);
    //Carga la etiqueta
    label.appendChild(span);
    label.appendChild(description);
    label.setAttribute("for", $id_value+$value);
    return label;
}

function crear_checkbox($id_value, $value, $checkbox_class){
    var checkbox = document.createElement("input");
    checkbox.setAttribute("id", $id_value+$value);
    checkbox.type = "checkbox";    
    checkbox.value = $value;
    checkbox.className += $checkbox_class;
    return checkbox;
}

// agrega checkbox a un div
function add_check_box($id_value, $value, $description, $div_container, $checkbox_class){

    var label = crear_flag_label($id_value, $value, $description, "list-group-item");
    var checkbox =  crear_checkbox($id_value, $value, $checkbox_class);
    document.getElementById($div_container).appendChild(checkbox);
    document.getElementById($div_container).appendChild(label);    
}


function crear_textbox($id, $textbox_class){
    var textbox = document.createElement("input");
    textbox.type = "text";    // make the element a checkbox
    textbox.setAttribute("id", $id);
    textbox.className += "form-control "+$textbox_class;
    textbox.addEventListener('keydown', realizar_conversion);
    return textbox; 
}

// crea la seccion del textbox para la conversion
function add_text_box($id, $div_container, $textbox_class){
    // div
    var div = document.createElement("div");
    div.setAttribute("class", "form-group row");     
    // div2
    var div2 = document.createElement("div");
    div2.className += "col-sm-5";   
    // textbox
    var textbox = crear_textbox($id, $textbox_class);
    div2.appendChild(textbox);   
    // label
    var label = crear_flag_label("", $id, " | "+$id, "col-sm-3 col-form-label");     
    div.appendChild(label);
    div.appendChild(div2);   
    document.getElementById($div_container).appendChild(div);
}

// --------------------- accion keydown --------------------
function ejecutar_api($target,$source ){
    if($target!== $source){
        var $quantity = document.getElementById($source).value;     
        var conversion = get_api($source, $target, $quantity); 
        var text = document.getElementById($target);
        text.value = conversion;  
        console.log("valor: "+conversion+" actual: "+$source+" set: "+$target); 
    }
 
}

function realizar_conversion(){
    $('.textbox_divisa:focus').each(
        function() {
            var id_current_input = $(this).attr('id');
            $('.textbox_divisa').each(
                function() {                    
                    ejecutar_api($(this).attr('id'),id_current_input);
                }
            );
        }
    );    
}