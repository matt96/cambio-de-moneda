// --------------------- manipular datos --------------------
const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

function get_conversion($source, $target, $quantity){

    var peticion = new XMLHttpRequest();    
    var url = 'https://free.currencyconverterapi.com/api/v6/convert?q='+$source+'_'+$target+'&compact=y';
    console.log(url);
    peticion.open('GET', url, false);
    peticion.send();
    var respuesta = peticion.responseText;
    var obj = get_json_obj(respuesta);
    console.log(obj);
    var cambio = get_cambio(obj);
    var conversion =  convertir_divisa(cambio, $quantity);
    return conversion;
    
}

function get_json_obj($respuesta){
    
    var obj = JSON.parse($respuesta);
    return obj;
}

function get_cambio($json){
    for(var divisa in $json){;
        for(var valor in $json[divisa]){
            return $json[divisa][valor];
        }
    }
}

function convertir_divisa($cambio, $cantidad){
    return ($cambio * $cantidad);
}

function ejecutar_api($path){
    console.log($path);
    var array = $path.split('&');
    console.log(array);
    var size = array.length;
    console.log(size);
    var target = array[size-2] ;
    var source =  array[size-3] ;
    var quantity = array[size-1] ;
    return get_conversion(source, target, quantity);
}

exports.ejecutar_api = ejecutar_api;